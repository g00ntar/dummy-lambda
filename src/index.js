async function handler(event) {
  const { queryStringParameters } = event;
  return {
    statusCode: 200,
    headers: {
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify(queryStringParameters)
  };
};
